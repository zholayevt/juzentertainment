//
//  SettingsTableViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 04.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    let cellCaregoryIdentifier = "CategoryCell"
    let cellLogoutIdentifier = "LogoutCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.title = "Настройки"
        tableView.register(SettingCategoryCell.self, forCellReuseIdentifier: cellCaregoryIdentifier)
        tableView.register(SettingLogoutCell.self, forCellReuseIdentifier: cellLogoutIdentifier)
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return section == 0 ? 3 : 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellCaregoryIdentifier, for: indexPath) as! SettingCategoryCell
            cell.selectionStyle = .none
            if indexPath.row == 0 {
                cell.titleLabel.text = "Сменить язык"
                cell.selectedLabel.text = "Русский"
            }
            else if indexPath.row == 1 {
                cell.titleLabel.text = "Редактировать профиль"
            }
            else {
                cell.titleLabel.text = "О приложении"
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellLogoutIdentifier, for: indexPath) as! SettingLogoutCell
            cell.titleLabel.text = "Выйти"
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            cell.selectionStyle = .none
            return cell
        }
    
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let vc = SettingDetailViewController()
                vc.vcIdentifier = "Language"
                self.show(vc, sender: nil)
            case 1:
                let vc = EditProfileViewController()
                self.show(vc, sender: nil)
            case 2:
                let vc = SettingDetailViewController()
                vc.vcIdentifier = "About"
                self.show(vc, sender: nil)
            default:
                break
            }
        case 1:
            let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)
            let firstAction = UIAlertAction(title: "Выйти", style: .default, handler: { (act) in
                AuthHelper.shared.logout()
            })
            let secondAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
            alert.addAction(firstAction)
            alert.addAction(secondAction)
            present(alert, animated: true, completion: nil)
        default:
            break
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 32.0
    }
    
    


}
class SettingCategoryCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let titleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        return label
    }()
    
    let selectedLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        label.text = ""
        return label
    }()
    
    let nextIcon: UIImageView = {
        let image = UIImageView(image: UIImage(named: "icon_next"))
        return image
    }()
    
    func setupViews() {
        backgroundColor = .white
        addSubview(titleLabel)
        addSubview(selectedLabel)
        addSubview(nextIcon)
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16.0)
            make.centerY.equalToSuperview()
        }
        nextIcon.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16.0)
            make.centerY.equalToSuperview()
        }
        selectedLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(nextIcon).offset(-8.0)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.right.equalTo(selectedLabel).offset(-8.0)
        }
    }
}
class SettingLogoutCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let titleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        label.textColor = UIColor(red:0.80, green:0.03, blue:0.08, alpha:1.00)
        
        return label
    }()

    
    func setupViews() {
        backgroundColor = .white
        addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
       
    }
}
