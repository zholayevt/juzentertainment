//
//  HeaderShopTableViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat on 09.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//
// Programmatically UI
import UIKit

class HeaderShopTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var pageControl: UIPageControl!
    var collectionView: UICollectionView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0.0)

        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.isPagingEnabled = true
        collectionView.register(HeaderShopCollectionViewCell.self, forCellWithReuseIdentifier: "HeaderShopCell")
        addSubview(collectionView)
        
        collectionView.snp.makeConstraints { (make) in
            make.left.equalTo(0.0)
            make.top.equalTo(0.0)
            make.right.equalTo(0.0)
            make.bottom.equalTo(0.0)
        }
        
        pageControl = UIPageControl(frame: CGRect(x: 0, y: 0, width: 60.0, height: 30.0))
        pageControl.numberOfPages = 2
        pageControl.currentPage = 0
        pageControl.pageIndicatorTintColor = .black
        addSubview(pageControl)
        pageControl.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(collectionView.snp.bottom)
        }
        
        collectionView.dataSource = self
        collectionView.delegate = self
 
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
     
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: CollectionView DataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderShopCell", for: indexPath) as! HeaderShopCollectionViewCell
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    
}
