//
//  CityCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat on 04.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class CityCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var date: UILabel!
    
}
