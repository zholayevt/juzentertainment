//
//  ViewController+Extension.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 25.09.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

extension UIViewController {
    
    //MARK: - Navigation bar functions
    func setEmptyBackTitle() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //self.navigationController?.navigationBar.tintColor = .black
    }
    func changeNavBarDissapear() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
        self.navigationController?.view.backgroundColor = .white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = nil
    }
    func transparetNavBarDidLoad() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    // MARK: - Registering Xib files
    func delegateAndRegister(cv:UICollectionView, id:String, nibName:String){
        cv.register(UINib.init(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: id)
        cv.delegate = self as? UICollectionViewDelegate
        cv.dataSource = self as? UICollectionViewDataSource
        
    }
    
    // MARK: - Keyboard callbacks
    func keyboardWillShow(sender: Notification) {
        self.view.frame.origin.y = -50
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    func keyboardWillHide(sender: Notification) {
        self.view.frame.origin.y = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: - Convert color to UIImage (for navigation bar)
    func imageFromColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIGraphicsBeginImageContext(size)
        image?.draw(in: rect)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}
