//
//  AllMusicViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 13.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class AllMusicVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    var searchController : UISearchController!
    var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Все песни"
       
        setupViews()
    }
    
    func setupViews(){
         view.backgroundColor = .white
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame:  CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height), collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalTo(16.0)
        }
        delegateAndRegister(cv: collectionView, id: trackIdentifier, nibName: "TrackCollectionViewCell")
        setupSearchBar()
    }
    
    func setupSearchBar() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: nil)
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.isTranslucent = false
        self.searchController = UISearchController(searchResultsController:  nil)
        //self.searchController.searchResultsUpdater = self
        //self.searchController.delegate = self
        //self.searchController.searchBar.delegate = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            // Fallback on earlier versions
            self.navigationItem.titleView = searchController.searchBar
        }
        self.definesPresentationContext = true
        let searchTextField = searchController.searchBar.value(forKey: "_searchField") as? UITextField
        searchTextField?.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: trackIdentifier, for: indexPath) as! TrackCollectionViewCell
        cell.moreButton.addTarget(self, action: #selector(moreTrackButtonPressed(sender:)), for: .touchUpInside)
        cell.moreButton.tag = indexPath.row
        
        
        let selectionView = UIView()
        selectionView.backgroundColor = UIColor.white.withAlphaComponent(0.45)
        cell.selectedBackgroundView = selectionView
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 54.0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    func moreTrackButtonPressed(sender:UIButton) {
        let alert = UIAlertController(title: "Выберите действие", message: "Track", preferredStyle: .actionSheet)
        let downloadAction = UIAlertAction(title: "Скачать", style: .default) { (act) in
            
        }
        let favoriteAction = UIAlertAction(title: "Добавить в избранное", style: .default, handler: nil)
        let albumAction = UIAlertAction(title: "Смотреть альбом", style: .default, handler: nil)
        let shareAction = UIAlertAction(title: "Поделиться", style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(downloadAction)
        alert.addAction(favoriteAction)
        alert.addAction(albumAction)
        alert.addAction(shareAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }

}
