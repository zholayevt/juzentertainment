//
//  ConcertCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 25.09.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class ConcertCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bgImage: GradientGreenBottomView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    var needToRound: Bool = false {
        didSet {
            if needToRound {
                bgImage.layer.cornerRadius = 5.0
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
