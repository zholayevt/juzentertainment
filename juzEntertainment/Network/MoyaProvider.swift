//
//  MoyaAdapter.swift
//  juzEntertainment
//
//  Created by Toremurat on 03.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import Foundation
import UIKit
import Moya

struct NetworkAdapterSigned {
    static let provider = MoyaProvider<APIEndpoint>()
    
    static func request(target: APIEndpoint, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) {
        
    }
}
