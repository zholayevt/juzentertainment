//
//  APIManager.swift
//  juzEntertainment
//
//  Created by Toremurat on 03.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Moya
import ObjectMapper

enum MyAPI {
    // MARK: - User
    case createUser(name: String, email:String, password:String)
    case changePassword(currentPassword: String, newPassword: String)
    case resetPasswordForEmail(email: String)
    
    // MARK: - Tour
    case getClips
    case getArtists
}

extension MyAPI: TargetType {
   
    var baseURL: URL { return URL(string: "http://91.bugingroup.com")! }
    
    var method: Moya.Method {
        switch self {
        case .createUser:
            return .post
        default:
            return .get
        }
    }
    var path: String {
        switch self {
        case .createUser:
            return "/user"
        case .getClips:
            return "/api/ru/clips"
        case .getArtists:
            return "/api/ru/bands"
        default:
            return ""
        }
    }
    var headers: [String : String]? {
        //any headers are welcome here:)
        return nil
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .createUser(let name, let email, let password):
            var parameters = [String: Any]()
            parameters["name"] = name
            parameters["email"] = email
            parameters["plain_password"] = password
            return parameters
        //any other params here
        default:
            return nil
        }
    }
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
}
