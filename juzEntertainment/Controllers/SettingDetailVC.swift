//
//  SettingListViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 11.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class SettingDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var vcIdentifier = ""
    var langs = ["Қазақша", "Русский", "English"]
    var tableView: UITableView!
    var aboutLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        switch vcIdentifier {
        case "Language":
            setupLanguageView()
        case "About":
            setupAboutView()
        default:
            break
        }
        // Do any additional setup after loading the view.
    }
    func setupLanguageView(){
        self.title = "Выберите язык"
        tableView = UITableView()
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    func setupAboutView() {
        self.title = "О приложении"
        aboutLabel = UILabel()
        aboutLabel.text = "The states of a user interface work together to communicate to your customer what they should be doing next, what their expectations should be at a given moment, and how they should be using a screen at any given time. But this discussion left out a key factor: how do you effectively travel between each state? How do you communicate what’s happening without displaying each event like some logfile or something?"
        aboutLabel.numberOfLines = 999
        aboutLabel.translatesAutoresizingMaskIntoConstraints = true
        aboutLabel.font = UIFont(name: "SFUIText-Regular", size: 14.0)
        view.addSubview(aboutLabel)
        aboutLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16.0)
            make.top.equalToSuperview().offset(32.0)
            make.right.equalToSuperview().offset(16.0)
        }
    }
    
    
    // MARK: ---------------------------Language Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = langs[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    

}
