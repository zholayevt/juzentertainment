//
//  MemberCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 12.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class MemberCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var nickLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.layer.cornerRadius =  65.0
        imageView.clipsToBounds = true
    }

}
