//
//  PlaceCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 02.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class PlaceCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    var bgColor: UIColor = .blue
    var isRow: Bool = false {
        didSet {
            if isRow {
                contentView.backgroundColor = .white
                titleLabel.textColor = .black
            }
            else {
                contentView.backgroundColor = bgColor
                titleLabel.textColor = .white
            }
        }
    }
}
