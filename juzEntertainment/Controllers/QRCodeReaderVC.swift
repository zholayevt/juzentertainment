//
//  QRCodeViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 04.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit
import AVFoundation
class QRCodeVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var qrCodeDetectionImage: UIImageView!
    var qrCodeDetected: UIImageView?
    
    var qrDispayView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews(
        )
        view.backgroundColor = .white
        
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
        } catch {
            print(error)
            return
        }
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer?.frame = qrDispayView.layer.bounds
        qrDispayView.layer.addSublayer(videoPreviewLayer!)
        captureSession?.startRunning()
        
        
        
    }
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        if metadataObjects == nil || metadataObjects.count == 0 {
            print("no qr detected")
            qrCodeDetectionImage?.image = UIImage(named: "img_focus")
            return
        }
        if let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
            if metadataObj.type == AVMetadataObjectTypeQRCode {
                
                let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
                print(metadataObj.stringValue)
                qrCodeDetectionImage?.image = UIImage(named: "img_focus_done")
                
                if metadataObj.stringValue != nil {
                    print("it is not qr code")
                    //qrCodeDetectionImage?.image = UIImage(named: "img_subscribe")
                }
            }
            else {
                qrCodeDetectionImage?.image = UIImage(named: "img_focus")
            }
        }
    }
    
    
    func setupViews(){
        qrDispayView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        let messageLabel: UILabel = {
            let label = UILabel()
            label.font = UIFont(name: "SFUIText-Regular", size: 17.0)
            label.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
            label.text = "Наведите на QR-code указанный на билета"
            label.numberOfLines = 2
            label.textAlignment = .center
            return label
        }()
        view.addSubview(messageLabel)
        view.addSubview(qrDispayView)
        
        //Custom [] frame for QR Camera
        qrCodeDetectionImage = UIImageView(image: UIImage(named: "img_focus"))
        view.addSubview(qrCodeDetectionImage)
        view.bringSubview(toFront: qrCodeDetectionImage)
        
        qrCodeDetectionImage.snp.makeConstraints { (make) in
            make.centerX.equalTo(qrDispayView)
            make.centerY.equalTo(qrDispayView)
            
        }
        messageLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(32.0)
            make.right.equalToSuperview().offset(-32.0)
            make.top.equalToSuperview().offset(16.0)
        }
       
        qrDispayView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalTo(messageLabel.snp.bottom).offset(16.0)
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
       
    }
    
    
}
