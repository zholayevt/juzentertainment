//
//  CategoryShopCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat on 10.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class CategoryShopCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
