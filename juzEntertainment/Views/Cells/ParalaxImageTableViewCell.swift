//
//  ParalaxImageTableViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 27.09.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class ParalaxImageTableViewCell: UITableViewCell {
 
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y >= 0 {
            // scrolling down
            containerView.clipsToBounds = true
            bottomSpaceConstraint?.constant = -scrollView.contentOffset.y / 2
            topSpaceConstraint?.constant = scrollView.contentOffset.y / 2
        } else {
            // scrolling up
            
            //topSpaceConstraint?.constant = scrollView.contentOffset.y
            containerView.clipsToBounds = false
        }
    }
    
    
        
    

}
