//
//  ProfileViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 29.09.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//  ANY QUESTIONS? @zholayev - Telegram

import UIKit
import SwiftyJSON
import SVProgressHUD

let profilePs = "img_ava"

class ProfileViewController: UIViewController {
    var headerView: UIView!
    var subscribeView: UIView!
    var menuView: UIView!
    var userNameLabel: UILabel!
    var emailLabel: UILabel!
    var profileImage: UIImageView!
    
    let viewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setEmptyBackTitle()
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.barTintColor = .white
        setupNavigationBar()
        setupViews()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupData()
    }
    func setupNavigationBar() {
        self.title = "Профиль"
        let settingsItem = UIBarButtonItem(image: UIImage(named:"icon_settings"), style: .plain, target: self, action: #selector(settingsBarButtonPressed))
        self.navigationItem.rightBarButtonItem = settingsItem
        
        self.navigationController?.navigationBar.isTranslucent = false
    }
    func setupViews() {
        view.backgroundColor = .white
        setHeaderView()
        setSubscribeView(active: false)
        setupMenuView()
    }
    
    func settingsBarButtonPressed() {
        let vc = SettingsTableViewController()
        self.show(vc, sender: nil)
    }
}
extension ProfileViewController {
    
    func setupData() {
        if let token = UserDefaults.standard.string(forKey: "token") {
            APIManager.makesRequest(target: .profileUpdate(token: token, name: ""), success: { [weak self] (json) in
                print(json)
                guard let vc = self, let email = json["result"]["email"].string,
                    let name = json["result"]["name"].string else { return }
                vc.emailLabel.text = email
                vc.userNameLabel.text = name
                if let imageUrl = json["result"]["image"].string {
                    vc.profileImage.sd_setImage(with: URL(string: baseUrlString + imageUrl), placeholderImage: UIImage(named: profilePs))
                }
            })
        }
        else {
            //here go to authorization state
            SVProgressHUD.show(UIImage(named: ""), status: BaseMessage.authorizeAgain)
        }
    }
    
    // MARK: ---------------------------set up header view of profile
    func setHeaderView() {
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80.0))
        profileImage = UIImageView(image: UIImage(named: "img_ava"))
        profileImage.contentMode = .scaleAspectFill
        profileImage.clipsToBounds = true
        profileImage.layer.cornerRadius = 30.0
        headerView.addSubview(profileImage)
        
        profileImage.snp.makeConstraints { (make) in
            make.left.equalTo(headerView).offset(16.0)
            make.centerY.equalTo(headerView)
            make.height.equalTo(60.0)
            make.width.equalTo(60.0)
        }
        
        userNameLabel = UILabel()
        userNameLabel.text = "Профиль"
        userNameLabel.font = UIFont(name: "SFUIText-Regular", size: 14.0)
        
        emailLabel = UILabel()
        emailLabel.text = "Email"
        emailLabel.font = UIFont(name: "SFUIText-Regular", size: 14.0)
        
        headerView.addSubview(userNameLabel)
        headerView.addSubview(emailLabel)
        
        userNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(profileImage.snp.right).offset(16.0)
            make.right.equalTo(headerView.snp.right).offset(-16.0)
            make.centerY.equalTo(profileImage).offset(-8)
        }
        emailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(profileImage.snp.right).offset(16.0)
            make.right.equalTo(headerView.snp.right).offset(-16.0)
            make.top.equalTo(userNameLabel.snp.bottom).offset(8.0)
        }
        
        view.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview().offset(0.0)
            make.height.equalTo(80.0)
        }
        
        let line = UIView()
        line.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        view.addSubview(line)
        line.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
            make.height.equalTo(1.0)
        }
    }
    // MARK: ---------------------------set up subscribe view (2 options)
    func setSubscribeView(active:Bool) {
        subscribeView = UIView()

        let titleLabel = UILabel()
        titleLabel.text = active ? "Вы подписаны" : "Песни без ограничения"
        titleLabel.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        subscribeView.addSubview(titleLabel)
        
        let descLabel = UILabel()
        descLabel.text = active ? BaseMessage.subscribeTrue : BaseMessage.subscribeFalse
        descLabel.font = UIFont(name: "SFUIText-Regular", size: 13.0)
        descLabel.numberOfLines = 5
        descLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.54)
        subscribeView.addSubview(descLabel)
        
        let image = UIImageView(image: UIImage(named: active ? "img_subscribe_done" : "img_subscribe"))
        image.contentMode = .scaleAspectFit
        image.clipsToBounds = true
        subscribeView.addSubview(image)
        
        let button = UIButton()
        button.setTitle(active ? "Отписаться" : "Оформить подписку (2.9$/мес)", for: .normal)
        button.titleLabel?.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        button.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        button.layer.borderWidth = 1.0
        button.addTarget(self, action: #selector(subscribeButtonPressed(active:)), for: .touchUpInside)
        button.backgroundColor = active ? UIColor.white : UIColor(red:0.44, green:0.73, blue:0.15, alpha:1.0)
        button.setTitleColor(active ? .black : .white, for: .normal)
        subscribeView.addSubview(button)
        
        view.addSubview(subscribeView)
        //contraints
        subscribeView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
            
        }
        button.snp.makeConstraints { (make) in
            make.left.equalTo(subscribeView).offset(-1.0) // because i made border width for button 1.0 px
            make.right.equalTo(subscribeView).offset(1.0) // out of presenting view
            make.bottom.equalTo(subscribeView)
            make.height.equalTo(44.0)
            
        }
        image.snp.makeConstraints { (make) in
            make.right.equalTo(subscribeView).offset(-16.0)
            make.top.equalTo(subscribeView).offset(16.0)
            make.bottom.equalTo(button.snp.top).offset(-16.0)
            make.width.equalTo(image.snp.width).multipliedBy(1)
            make.height.equalTo(image.snp.height).multipliedBy(1)
            
        }
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(subscribeView).offset(16.0)
            make.right.equalTo(image.snp.left).offset(-16.0)
            make.top.equalTo(subscribeView).offset(12.0)
        }
        descLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(3.0)
            make.trailing.equalTo(titleLabel)
            make.leading.equalTo(titleLabel)
        }
    }
    func subscribeButtonPressed(active:Bool){
        print(active)
    }
    
    // MARK: ---------------------------set up menu view of profile
    func setupMenuView() {
        menuView = UIView()
        menuView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        menuView.layer.borderWidth = 1.0
        
        let buttonDown = UIButton()
        let buttonFav = UIButton()
        let buttonCart = UIButton()
        buttonDown.addTarget(self, action: #selector(downloadButtonPressed), for: .touchUpInside)
        buttonCart.addTarget(self, action: #selector(cartButtonPressed), for: .touchUpInside)
        
        
        menuView.addSubview(buttonDown)
        menuView.addSubview(buttonFav)
        menuView.addSubview(buttonCart)

        let downIcon = UIImageView()
        let nextDownIcon = UIImageView()
        let downloadLabel = UILabel()
        downloadLabel.text = "Скачанные песни"
        downloadLabel.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        downIcon.image = UIImage(named: "icon_download")
        nextDownIcon.image = UIImage(named: "icon_next")
        
        buttonDown.addSubview(downIcon)
        buttonDown.addSubview(downloadLabel)
        buttonDown.addSubview(nextDownIcon)
        
        let favIcon = UIImageView()
        let nextFavIcon = UIImageView()
        let favoriteLabel = UILabel()
        favoriteLabel.text = "Все избранные"
        favoriteLabel.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        favIcon.image = UIImage(named: "icon_favorite")
        nextFavIcon.image = UIImage(named: "icon_next")
        
        buttonFav.addSubview(favIcon)
        buttonFav.addSubview(favoriteLabel)
        buttonFav.addSubview(nextFavIcon)
        
        let cartIcon = UIImageView()
        let nextCartIcon = UIImageView()
        let cartLabel = UILabel()
        cartLabel.text = "Покупки"
        cartLabel.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        cartIcon.image = UIImage(named: "icon_cart")
        nextCartIcon.image = UIImage(named: "icon_next")
        
        buttonCart.addSubview(cartIcon)
        buttonCart.addSubview(cartLabel)
        buttonCart.addSubview(nextCartIcon)
        
        
        view.addSubview(menuView)
        menuView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(-1.0)
            make.right.equalToSuperview().offset(1.0)
            make.top.equalTo(subscribeView.snp.bottom).offset(32.0)
            make.height.equalTo(44*3)
        }
        buttonDown.snp.makeConstraints { (make) in
            make.left.equalTo(menuView)
            make.right.equalTo(menuView)
            make.top.equalTo(menuView)
            make.height.equalTo(44.0)
        }
        buttonFav.snp.makeConstraints { (make) in
            make.left.equalTo(menuView)
            make.top.equalTo(buttonDown.snp.bottom)
            make.right.equalTo(menuView)
            make.height.equalTo(44.0)
        }
        buttonCart.snp.makeConstraints { (make) in
            make.left.equalTo(menuView)
            make.top.equalTo(buttonFav.snp.bottom)
            make.right.equalTo(menuView)
            make.height.equalTo(44.0)
        }
        
        //1
        downIcon.snp.makeConstraints { (make) in
            make.left.equalTo(menuView).offset(16.0)
            make.centerY.equalTo(buttonDown)
        }
        downloadLabel.snp.makeConstraints { (make) in
            make.left.equalTo(downIcon.snp.right).offset(16.0)
            make.centerY.equalTo(buttonDown)
        }
        nextDownIcon.snp.makeConstraints { (make) in
            make.right.equalTo(menuView).offset(-16.0)
            make.centerY.equalTo(buttonDown)
        }
        //2
        favIcon.snp.makeConstraints { (make) in
            make.left.equalTo(menuView).offset(16.0)
            make.centerY.equalTo(buttonFav)
        }
        favoriteLabel.snp.makeConstraints { (make) in
            make.left.equalTo(downIcon.snp.right).offset(16.0)
            make.centerY.equalTo(buttonFav)
        }
        nextFavIcon.snp.makeConstraints { (make) in
            make.right.equalTo(menuView).offset(-16.0)
            make.centerY.equalTo(buttonFav)
        }
        //3
        cartIcon.snp.makeConstraints { (make) in
            make.left.equalTo(menuView).offset(16.0)
            make.centerY.equalTo(buttonCart)
        }
        cartLabel.snp.makeConstraints { (make) in
            make.left.equalTo(downIcon.snp.right).offset(16.0)
            make.centerY.equalTo(buttonCart)
        }
        nextCartIcon.snp.makeConstraints { (make) in
            make.right.equalTo(menuView).offset(-16.0)
            make.centerY.equalTo(buttonCart)
        }
      
    }
    
    // MARK: --------------------------- UIButton Actions
    
    func downloadButtonPressed() {
        let vc = QRCodeViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func cartButtonPressed() {
        let vc = PurchasesPageViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

