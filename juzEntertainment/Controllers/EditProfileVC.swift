//
//  EditProfileViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 11.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        return scroll
    }()
    let contentView: UIView = {
        let content = UIView()
        content.backgroundColor = .white
        return content
    }()
    let imagePicker = UIImagePickerController()
    var usernameLabel:UILabel!
    var usernameTextField:UITextField!
    var avatarView: UIImageView!
    var chooseButton: UIButton!
    var saveButton: UIButton!
    
    let viewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        self.title = "Редактирование"
        imagePicker.delegate = self
        setupData()
        
    }
    func setupData() {
        let token = UserDefaults.standard.string(forKey: "token")!
        APIManager.makesRequest(target: .profileUpdate(token: token, name: "")) { [weak self] (json) in
            guard let vc = self,
                let name = json["name"].string,
                let url = json["image"].string
                else { return }
            vc.usernameTextField.text = name
            vc.avatarView.sd_setImage(with: URL(string: baseUrlString + url), placeholderImage: UIImage(named: profilePs))
        }
    }
    func setupViews() {
        usernameLabel = UILabel()
        usernameLabel.text = "ИМЯ ПОЛЬЗОВАТЕЛЯ"
        usernameLabel.font = UIFont(name: "SFUIText-Regular", size: 12.0)
        usernameLabel.textColor = UIColor.gray
        
        usernameTextField = UITextField()
        usernameTextField.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Имя")
        usernameTextField.autocorrectionType = .no
        usernameTextField.borderStyle = .none
        
        let avatarGesture = UIGestureRecognizer(target: self, action: #selector(avatarGestureAction))
        avatarView = UIImageView()
        avatarView.image = UIImage(named: "img_ava")
        avatarView.clipsToBounds = true
        avatarView.contentMode = .scaleAspectFill
        avatarView.layer.cornerRadius = 70/2
        avatarView.addGestureRecognizer(avatarGesture)
        
        chooseButton = UIButton(type: .custom)
        chooseButton.setTitle("Изменить", for: .normal)
        chooseButton.setTitleColor(UIColor(red:0.09, green:0.45, blue:0.91, alpha:1.00), for: .normal)
        chooseButton.titleLabel?.font = UIFont(name: "SFUIText-Regular", size: 16.0)
        chooseButton.addTarget(self, action: #selector(avatarGestureAction), for: .touchUpInside)
        
        saveButton = UIButton(type: .custom)
        saveButton.setTitle("Сохранить", for: .normal)
        saveButton.setTitleColor(UIColor.white, for: .normal)
        saveButton.titleLabel?.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
        saveButton.backgroundColor = UIColor(red:0.09, green:0.45, blue:0.91, alpha:1.00)
        
        contentView.addSubview(saveButton)
        contentView.addSubview(avatarView)
        contentView.addSubview(usernameLabel)
        contentView.addSubview(chooseButton)
        contentView.addSubview(usernameTextField)

        
        makeConstaints()
       
        
        
    }
    func makeConstaints() {
        let grayLine: UIView = {
            let line = UIView(frame: CGRect(x: 16.0, y: 0, width: view.frame.width, height: 1.0))
            line.backgroundColor = UIColor.gray
            return line
        }()
        
        contentView.addSubview(grayLine)
        scrollView.addSubview(contentView)
        view.addSubview(scrollView)
        
        scrollView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        contentView.snp.makeConstraints { (make) in
            make.left.equalTo(scrollView)
            make.top.equalTo(scrollView)
            make.width.equalToSuperview()
            make.right.equalTo(scrollView)
            make.bottom.equalTo(scrollView)
            make.height.equalTo(800.0)
        }
        avatarView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(contentView).offset(32.0)
            make.height.equalTo(70.0)
            make.width.equalTo(70.0)
        }
        chooseButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(contentView)
            make.top.equalTo(avatarView.snp.bottom).offset(8.0)
        }
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(chooseButton.snp.bottom).offset(32.0)
            make.left.equalTo(contentView).offset(16.0)
            make.right.equalTo(contentView).offset(-16.0)
            
        }
        usernameTextField.snp.makeConstraints { (make) in
            make.top.equalTo(usernameLabel.snp.bottom).offset(8.0)
            make.trailing.equalTo(usernameLabel)
            make.leading.equalTo(usernameLabel)
            make.height.equalTo(44.0)
        }
       
        grayLine.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(16.0)
            make.right.equalTo(contentView).offset(-16.0)
            make.top.equalTo(usernameTextField.snp.bottom).offset(0.0)
            make.height.equalTo(1.0)
        }
        saveButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(grayLine.snp.bottom).offset(32)
            make.height.equalTo(44.0)
        }
        
        
    }
    func saveButtonPressed() {
        guard let username = usernameTextField.text, !username.isEmpty else {
            SVProgressHUD.showError(withStatus: BaseMessage.fillEverything)
            return
        }
        let token = UserDefaults.standard.string(forKey: "token")!
        let vm = APIManager()
        vm.updateProfileImage(token: token, name: username, image: avatarView.image!) { (json) in
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func avatarGestureAction(){
        let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)
        let changeAction = UIAlertAction(title: "Выбрать", style: .default) { (act) in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let deleteAction = UIAlertAction(title: "Удалить", style: .default) { (act) in
            
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(changeAction)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
 
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            avatarView.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
