//
//  HeaderShopCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat on 09.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class HeaderShopCollectionViewCell: UICollectionViewCell {
    
    var bgImage: GradientBlackBottomView!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        bgImage = GradientBlackBottomView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        bgImage.contentMode = .scaleAspectFill
        bgImage.clipsToBounds = true
        bgImage.image = UIImage(named: pcImageName)
        addSubview(bgImage)
        bgImage.snp.makeConstraints { (make) in
            make.left.equalTo(0.0)
            make.right.equalTo(0.0)
            make.bottom.equalTo(0.0)
            make.top.equalTo(0.0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
       
    }
}
