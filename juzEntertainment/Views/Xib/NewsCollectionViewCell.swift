//
//  NewsCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 26.09.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var bgImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
