//
//  ArtistCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 12.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class ArtistCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bgImage: GradientBlackBottomView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
