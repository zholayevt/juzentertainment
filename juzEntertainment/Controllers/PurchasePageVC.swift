//
//  OrdersPageViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 11.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class PurchasesPageVC: UIViewController {

    var addButton:UIBarButtonItem!
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPageMenu()
        self.title = "Покупки"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.navigationBar.isTranslucent = false
        super.viewWillAppear(animated)
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        //self.navigationController?.navigationBar.isTranslucent = true
        super.viewWillDisappear(animated)
    }
    func buttonMethod() {
        print("Perform action")
    }
    
    func setupPageMenu(){
        // MARK: - UI Setup
        self.navigationController?.navigationBar.isOpaque = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
        
        // MARK: - Scroll menu setup
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let controller1 = PurchasesTicketsVC()
        controller1.title = "Билеты"
        controllerArray.append(controller1)
        
        let controller2 = PurchasesOrdersVC()
        controller2.title = "Товары"
        controllerArray.append(controller2)
        
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .menuMargin(5),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuItemFont(UIFont.systemFont(ofSize: 16.0)),
            .menuHeight(33.0),
            .menuItemWidth(self.view.frame.width/2-10),
            .addBottomMenuHairline(true),
            .bottomMenuHairlineColor(UIColor(red:0.92, green:0.92, blue:0.95, alpha:1.0)),
            .enableHorizontalBounce(false),
            .menuItemFont(UIFont(name: "SFUIText-Regular", size: 17.0)!),
            .selectionIndicatorHeight(2.0),
            
            ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController: self)
    }
//    func registerPushNotifications() {
//        DispatchQueue.main.async {
//            if #available(iOS 10.0, *) {
//                let center = UNUserNotificationCenter.current()
//                center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
//                    // actions based on whether notifications were authorized or not
//                }
//                UIApplication.shared.registerForRemoteNotifications()
//                
//                
//            } else {
//                let setting = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//                UIApplication.shared.registerUserNotificationSettings(setting)
//                UIApplication.shared.registerForRemoteNotifications()
//            }
//            
//            
//            
//        }
//    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }

}
