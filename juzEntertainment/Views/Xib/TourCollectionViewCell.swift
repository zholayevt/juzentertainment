//
//  TourCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 25.09.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class TourCollectionViewCell: UICollectionViewCell {
    var needToRound: Bool = false {
        didSet {
            if needToRound {
                 bgImage.layer.cornerRadius = 10.0
            }
        }
    }
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var bgImage: GradientBlueBottomView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
