//
//  MusicCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 13.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class MusicCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var bgImage: GradientBlackBottomView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
