//
//  AllAlbumTVC.swift
//  juzEntertainment
//
//  Created by Toremurat on 10.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class AllAlbumTVC: UITableViewController {

    @IBOutlet weak var allCollectionView: UICollectionView!
    @IBOutlet weak var newCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Все альбомы"
        delegateAndRegister(cv: allCollectionView, id: "AlbumCell", nibName: "AlbumCollectionViewCell")
        delegateAndRegister(cv: newCollectionView, id: "AlbumCell", nibName: "AlbumCollectionViewCell")
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return indexPath.row == 0 ? 44.0 : indexPath.row == 1 ? 200.0 : 32.0
        default:
            let division = CGFloat(5/2 + 5%2)
            return indexPath.row == 0 ? 44.0 : indexPath.row == 1 ? (division * newCollectionView.frame.height) : 32.0
        }
    }
}
extension AllAlbumTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case newCollectionView:
            return 5
        default:
            return 5
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case newCollectionView:
            let cell = newCollectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCell", for: indexPath) as! AlbumCollectionViewCell
            return cell
        default:
            let cell = allCollectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCell", for: indexPath) as! AlbumCollectionViewCell
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case newCollectionView:
             return CGSize(width: 150.0, height: newCollectionView.frame.height)
        default:
             return CGSize(width: allCollectionView.frame.width * 0.48, height: newCollectionView.frame.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}
