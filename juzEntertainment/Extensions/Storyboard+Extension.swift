//
//  Storyboard+Extension.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 24.07.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
    
    class func tour() -> UIStoryboard {
        return UIStoryboard(name: "Tour", bundle: nil)
    }
    class func music() -> UIStoryboard {
        return UIStoryboard(name: "Music", bundle: nil)
    }
    class func auth() -> UIStoryboard {
        return UIStoryboard(name: "Auth", bundle: nil)
    }
    class func store() -> UIStoryboard {
        return UIStoryboard(name: "Store", bundle: nil)
    }
    class func profile() -> UIStoryboard {
        return UIStoryboard(name: "Profile", bundle: nil)
    }
    class func artist() -> UIStoryboard {
        return UIStoryboard(name: "Artists", bundle: nil)
    }
    class func initTourWith(identifier:String) -> UIViewController {
        return tour().instantiateViewController(withIdentifier: identifier)
    }
    class func initMusicWith(identifier:String) -> UIViewController {
        return music().instantiateViewController(withIdentifier: identifier)
    }
    class func initAuthWith(identifier:String) -> UIViewController {
        return auth().instantiateViewController(withIdentifier: identifier)
    }
    class func initStoreWith(identifier:String) -> UIViewController {
        return store().instantiateViewController(withIdentifier: identifier)
    }
    class func initProfileWith(identifier:String) -> UIViewController {
        return profile().instantiateViewController(withIdentifier: identifier)
    }
    class func initArtist(identifier:String) -> UIViewController {
        return artist().instantiateViewController(withIdentifier: identifier)
    }
}
