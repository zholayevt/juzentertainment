
//
//  DetailGroupTableViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 12.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class DetailGroupT: UITableViewController {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var imageView: GradientBlackBottomView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var memberCollectionView: UICollectionView!
    
    var detail: Artist!
    var members: [Member]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transparetNavBarDidLoad()
        setupViews()
        setupData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        changeNavBarDissapear()
    }
    
    //MARK: - Setup VIEW & DATA
    func setupViews() {
        delegateAndRegister(cv: memberCollectionView, id: "MemberCell", nibName: "MemberCollectionViewCell")
        memberCollectionView.isScrollEnabled = false
        tableView.contentInset = UIEdgeInsets(top: -64, left: 0, bottom: 0, right: 0)
        tableView.estimatedRowHeight = 8.0
        tableView.separatorStyle = .none
    }
    func setupData() {
        nameLabel.text = detail.name
        descLabel.text = detail.innterDesc.htmlString
        imageView.sd_setImage(with: URL(string: detail.outerImage), placeholderImage: UIImage(named: pcImageName))
        iconView.sd_setImage(with: URL(string: detail.iconImage), placeholderImage: UIImage(named: pcImageName))
    }
    
    // MARK: - TableView DataSource and Delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return indexPath.row == 0 ? 270.0 : UITableViewAutomaticDimension
        case 1:
            return indexPath.row == 0 ? CGFloat(members.count) * 250.0 : 44.0 //(44.0 bottom margin)
        default:
            return UITableViewAutomaticDimension
        }
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.white
            headerTitle.textLabel?.font = UIFont(name: "SFUIText-Bold", size: 19.0)
            headerTitle.backgroundView?.backgroundColor = .black
        }
    }

}
extension DetailGroupTableViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return members.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = memberCollectionView.dequeueReusableCell(withReuseIdentifier: "MemberCell", for: indexPath) as! MemberCollectionViewCell
        cell.imageView.sd_setImage(with: URL(string: members[indexPath.row].outerImage), placeholderImage: UIImage(named: pcImageName))
        cell.nickLabel.text = members[indexPath.row].nick
        cell.fullnameLabel.text = members[indexPath.row].name

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width * 0.4, height: 220.0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
