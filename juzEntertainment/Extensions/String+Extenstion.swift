//
//  Data+Extenstion.swift
//  juzEntertainment
//
//  Created by Toremurat on 03.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import Foundation
import UIKit
extension String {
   
    var htmlAttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8), options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlString: String {
        return htmlAttributedString?.string ?? ""
    }
    func getDateString() -> String {
        let date = Date(timeIntervalSince1970: Double(self)!)
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        return dateFormatter.string(from: date)
    }
    func getShortDateString() -> String {
        let date = Date(timeIntervalSince1970: Double(self)!)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.YY"
        return dateFormatter.string(from: date)
    }
}
