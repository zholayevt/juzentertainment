//
//  VideoFrameCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 26.09.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class VideoFrameCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
