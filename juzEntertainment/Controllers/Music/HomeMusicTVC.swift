//
//  MusicHomeTableViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 13.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class MusicHomeTVC: UITableViewController {

    @IBOutlet weak var recentMusicCollectionView: UICollectionView!
    @IBOutlet weak var albumCollectionView: UICollectionView!
    @IBOutlet weak var allMusicCollectionView: UICollectionView!
    @IBOutlet weak var newMusicCollectionView: UICollectionView!
    var searchController : UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegateAndRegister(cv: newMusicCollectionView, id: "MusicCell", nibName: "MusicCollectionViewCell")
        delegateAndRegister(cv: allMusicCollectionView, id: trackIdentifier, nibName: "TrackCollectionViewCell")
        delegateAndRegister(cv: albumCollectionView, id: "AlbumCell", nibName: "AlbumCollectionViewCell")
        delegateAndRegister(cv: recentMusicCollectionView, id: trackIdentifier, nibName: "TrackCollectionViewCell")
        setupSearchBar()
    }
    func setupSearchBar() {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: nil)
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.isTranslucent = false
        self.searchController = UISearchController(searchResultsController:  nil)
//        self.searchController.searchResultsUpdater = self
//        self.searchController.delegate = self
//        self.searchController.searchBar.delegate = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
        let searchTextField = searchController.searchBar.value(forKey: "_searchField") as? UITextField
        searchTextField?.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)

    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
        
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return 4
        default:
            return 3
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            if indexPath.row == 0 {
                let vc = UIStoryboard.initMusicWith(identifier: "AllMusicVC") as! AllMusicVC
                self.show(vc, sender: nil)
            }
        default:
            break
        }
    }
    

}
extension MusicHomeTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case newMusicCollectionView:
            return 3
        case allMusicCollectionView:
            return 5
        case albumCollectionView:
            return 4
        case recentMusicCollectionView:
            return 3
        default:
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case newMusicCollectionView:
            let cell = newMusicCollectionView.dequeueReusableCell(withReuseIdentifier: "MusicCell", for: indexPath) as! MusicCollectionViewCell
            return cell
        case allMusicCollectionView:
            let cell = allMusicCollectionView.dequeueReusableCell(withReuseIdentifier: trackIdentifier, for: indexPath) as! TrackCollectionViewCell
            cell.moreButton.addTarget(self, action: #selector(moreTrackButtonPressed(sender:)), for: .touchUpInside)
            cell.moreButton.tag = indexPath.row
            return cell
        case albumCollectionView:
            let cell = albumCollectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCell", for: indexPath) as! AlbumCollectionViewCell
            return cell
        case recentMusicCollectionView:
            let cell = recentMusicCollectionView.dequeueReusableCell(withReuseIdentifier: trackIdentifier, for: indexPath) as! TrackCollectionViewCell
            cell.moreButton.addTarget(self, action: #selector(moreTrackButtonPressed(sender:)), for: .touchUpInside)
         
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    func moreTrackButtonPressed(sender:UIButton) {
        let alert = UIAlertController(title: "Выберите действие \(sender.tag)", message: "Альбом", preferredStyle: .actionSheet)
        let downloadAction = UIAlertAction(title: "Скачать", style: .default) { (act) in
            
        }
        let favoriteAction = UIAlertAction(title: "Добавить в избранное", style: .default, handler: nil)
        let albumAction = UIAlertAction(title: "Смотреть альбом", style: .default, handler: nil)
        let shareAction = UIAlertAction(title: "Поделиться", style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(downloadAction)
        alert.addAction(favoriteAction)
        alert.addAction(albumAction)
        alert.addAction(shareAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case newMusicCollectionView:
            break
        case allMusicCollectionView:
            break
        case albumCollectionView:
            break
        case recentMusicCollectionView:
            break
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case newMusicCollectionView:
            return CGSize(width: view.frame.size.width * 0.4, height: newMusicCollectionView.frame.size.height)
        case allMusicCollectionView:
            return CGSize(width: collectionView.frame.size.width, height: 54.0)
        case albumCollectionView:
            return CGSize(width: view.frame.size.width * 0.4, height: albumCollectionView.frame.size.height)
        case recentMusicCollectionView:
            return CGSize(width: collectionView.frame.size.width, height: 54.0)

        default:
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        switch collectionView {
        case allMusicCollectionView:
            return 0.0
        case recentMusicCollectionView:
            return 0.0
        default:
            return 3.0
        }
    }
    
    
    
}

