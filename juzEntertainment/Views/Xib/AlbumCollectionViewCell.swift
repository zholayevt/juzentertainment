//
//  AlbumCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 13.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.layer.cornerRadius = imageView.frame.width/2
    }

}
