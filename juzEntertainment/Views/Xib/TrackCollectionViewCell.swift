//
//  TrackCollectionViewCell.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 26.09.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class TrackCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
   
    @IBOutlet weak var moreButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
}
