//
//  MyTicketsViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 11.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit

class PurchasesTicketsVC: UITableViewController {
    let cellIdentifier = "MyTicketCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(MyTicketCell.self, forCellReuseIdentifier: cellIdentifier)
        // Do any additional setup after loading the view.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyTicketCell
        cell.numberLabel.text = "№ 552771"
        cell.priceLabel.text = "10 000T"
        cell.descLabel.text = "Новогодняя ночь с Ninety OneНовогодняя ночь с Ninety One"
        cell.dateLabel.text = "24.12.17"
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54.0
    }

}
class MyTicketCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let numberLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        return label
    }()
    let priceLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "SFUIText-Regular", size: 17.0)
        return label
    }()
    let descLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red:0, green:0, blue:0, alpha:0.54)
        label.font = UIFont(name: "SFUIText-Regular", size: 13.0)
        return label
    }()
    let dateLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "SFUIText-Regular", size: 13.0)
        label.textColor = UIColor(red:0, green:0, blue:0, alpha:0.54)
        return label
    }()
    let nextIcon:UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "icon_next")
        return img
    }()
    
    func setupViews() {
        backgroundColor = .white
        addSubview(numberLabel)
        addSubview(priceLabel)
        addSubview(descLabel)
        addSubview(dateLabel)
        addSubview(nextIcon)
        
        nextIcon.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16.0)
            make.centerY.equalToSuperview()
        }
        priceLabel.snp.makeConstraints { (make) in
            make.right.equalTo(nextIcon.snp.left).offset(-8.0)
            make.top.equalToSuperview().offset(8.0)
            
        }
        numberLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16.0)
            make.top.equalToSuperview().offset(8.0)
            make.right.lessThanOrEqualTo(priceLabel.snp.left)
        }
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(priceLabel.snp.bottom)
            make.right.equalTo(nextIcon.snp.left).offset(-8.0)
        }
        descLabel.snp.makeConstraints { (make) in
            make.top.equalTo(numberLabel.snp.bottom)
            make.left.equalToSuperview().offset(16.0)
            make.right.lessThanOrEqualTo(dateLabel.snp.left).offset(-8.0)
        }
        
        
    }
}
