//
//  ArtistHomeViewController.swift
//  juzEntertainment
//
//  Created by Toremurat Zholayev on 12.10.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

let pcImageName = "bg_tour" //placeholder image name

class ArtistHomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var collectionView: UICollectionView!
    
    let cellIdentifier = "ArtistCell"
    let viewModel = ArtistViewModel()
    var bands: [Artist] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setEmptyBackTitle()
        self.title = "Артисты"
        setupViews()
        setupData()
    }
    func setupData() {
        viewModel.getGroupList(lang: "ru") { [weak self] (json) in
            guard let vc = self else { return }
            for (_, subJson):(String, JSON) in json["result"] {
                if let new = Artist(json: subJson) {
                    vc.bands.append(new)
                }
            }
            vc.collectionView.reloadData()
        }
    }
    func setupViews() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height), collectionViewLayout: layout)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        delegateAndRegister(cv: collectionView, id: cellIdentifier, nibName: "ArtistCollectionViewCell")
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.barTintColor = .white
    }
    //MARK: - CollectionView Delegate and DataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bands.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ArtistCollectionViewCell
        cell.nameLabel.text = bands[indexPath.row].name
        cell.descLabel.text = bands[indexPath.row].outerDesc.htmlString
        cell.iconImage.sd_setImage(with: URL(string: bands[indexPath.row].iconImage), completed: nil)
        cell.bgImage.sd_setImage(with: URL(string: bands[indexPath.row].outerImage), placeholderImage: UIImage(named: pcImageName))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.initArtist(identifier: "DetailGroup") as! DetailGroupTVC
        vc.detail = bands[indexPath.row]
        vc.members = bands[indexPath.row].members
        self.show(vc, sender: nil)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height * 0.35)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
}

